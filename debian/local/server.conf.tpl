[options]
# specify an alternative addons path.; default = None
# addons_path = None
# set superadmin password; default = admin
admin_passwd = admin
assert_exit_level = error
cache_timeout = 100000
csv_internal_sep = ,
# specify the database host; default = False
db_host = _DBC_DBSERVER_
# specify the the maximum number of physical connections to posgresql;
# default = 64
db_maxconn = 64
# specify the database name; default = False
db_name = _DBC_DBNAME_
# specify the database password; default = False
db_password = _DBC_DBPASS_
# specify the database port; default = False
db_port = _DBC_DBPORT_
# specify the database user name; default = False
db_user = _DBC_DBUSER_
debug_mode = False
demo = {}
# specify the smtp email address for sending email; default = False
email_from = False
# use this for big data importation, if it crashes you will be able to
# continue at the current state. provide a filename to store
# intermediate importation states.; default = 
import_partial = 
# disable the ability to return the list of databases; default = True
list_db = True
# specify the level of the logging. accepted values: ['info',
# 'debug_rpc', 'warn', 'test', 'critical', 'debug_sql', 'error',
# 'debug', 'debug_rpc_answer', 'notset']; default = 20
log_level = info
# file where the server log will be stored; default = None
logfile = /var/log/openerp6/openerp6-server.log
login_message = False
# do not rotate the logfile; default = True
logrotate = True
# disable the netrpc protocol; default = True
netrpc = True
# specify the tcp ip address for the netrpc protocol; default = 
netrpc_interface = localhost
# specify the tcp port for the netrpc protocol; default = 8070
netrpc_port = 8070
# force a limit on the maximum age of records kept in the virtual
# osv_memory tables. this is a decimal value expressed in hours, and
# the default is 1 hour.; default = 1
osv_memory_age_limit = 1.0
# force a limit on the maximum number of records kept in the virtual
# osv_memory tables. the default is false, which means no count-based
# limit.; default = None
osv_memory_count_limit = False
# specify the pg executable path; default = None
pg_path = None
reportgz = False
# specify the certificate file for the ssl connection; default = server.cert
secure_cert_file = /etc/ssl/certs/ssl-cert-snakeoil.pem
# specify the private key file for the ssl connection; default = server.pkey
secure_pkey_file = /etc/ssl/private/ssl-cert-snakeoil.key
# specify the smtp password for sending email; default = False
smtp_password = False
# specify the smtp port; default = 25
smtp_port = 25
# specify the smtp server for sending email; default = localhost
smtp_server = localhost
# specify the smtp server support ssl or not; default = False
smtp_ssl = False
# specify the smtp username for sending email; default = False
smtp_user = False
# specify the directory containing your static html files (e.g
# '/var/www/'); default = None
static_http_document_root = None
# enable static http service for serving plain html files; default = False
static_http_enable = False
# specify the url root prefix where you want web browsers to access
# your static html files (e.g '/'); default = None
static_http_url_prefix = None
# send the log to the syslog server; default = False
syslog = False
# commit database changes performed by tests.; default = False
test_commit = False
# disable loading test files.; default = False
test_disable = False
# launch a yml test file.; default = False
test_file = False
# if set, will save sample of all reports in this directory.; default = False
test_report_directory = False
timezone = False
translate_modules = ['all']
without_demo = False
# disable the xml-rpc protocol; default = True
xmlrpc = True
# specify the tcp ip address for the xml-rpc protocol; default = 
xmlrpc_interface = localhost
# specify the tcp port for the xml-rpc protocol; default = 8069
xmlrpc_port = 8069
# disable the xml-rpc secure protocol; default = True
xmlrpcs = True
# specify the tcp ip address for the xml-rpc secure protocol; default = 
xmlrpcs_interface = localhost
# specify the tcp port for the xml-rpc secure protocol; default = 8071
xmlrpcs_port = 8071

