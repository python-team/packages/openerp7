# Reset addons_path to default
s/\(^addons_path\).*/\# \1 = None/
# Remove confusing options changing which
# is not required on debian
/^root_path.*/d
/^pidfile.*/d
/^stop_after_init.*/d
# dbconfig-common
s/\(^db_host\).*/\1 = _DBC_DBSERVER_/
s/\(^db_name\).*/\1 = _DBC_DBNAME_/
s/\(^db_password\).*/\1 = _DBC_DBPASS_/
s/\(^db_port\).*/\1 = _DBC_DBPORT_/
s/\(^db_user\).*/\1 = _DBC_DBUSER_/
# bind on localhost
s/\(.*_interface\).*/\1 = localhost/
# ssl certs
s/\(^secure_cert_file\).*/\1 = \/etc\/ssl\/certs\/ssl-cert-snakeoil.pem/
s/\(^secure_pkey_file\).*/\1 = \/etc\/ssl\/private\/ssl-cert-snakeoil.key/
