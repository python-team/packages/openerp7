import os
import sys

sys.path.append('/usr/share/openerp-web')

from cherrypy._cpconfig import as_dict
import openobject

config = as_dict('/etc/openerp-web/openerp-web.cfg')

if ('/' in config and 'tools.sessions.storage_type' in config['/']
      and config['/']['tools.sessions.storage_type'] == 'file'):
    storage_path = config['/']['tools.sessions.storage_path']
    for the_file in os.listdir(storage_path):
        file_path = os.path.join(storage_path, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception, e:
            print e

openobject.configure(config)
openobject.enable_static_paths() # serve static file via the wsgi server

application = openobject.application
