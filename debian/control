Source: openerp7
Section: net
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Dmitrijs Ledkovs <dmitrij.ledkov@ubuntu.com>,
 Tristan Hill <tristan.hill@credativ.co.uk>,
 Chris Halls <halls@debian.org>
Build-Depends: debhelper (>= 7.0.50~), python, python-setuptools, po-debconf
Build-Depends-Indep:
 python-libxslt1, python-lxml, python-pychart, python-pydot, python-psycopg2,
 python-reportlab
Standards-Version: 3.9.2
Homepage: http://www.openerp.com/
Vcs-Git: https://salsa.debian.org/python-team/packages/openerp7.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/openerp7

Package: openerp7
Architecture: all
Depends:
 ${python:Depends}, ${misc:Depends}, adduser, python, postgresql-client,
 python-dateutil, python-docutils, python-feedparser, python-gdata,
 python-imaging, python-jinja2, python-ldap, python-libxslt1, python-lxml,
 python-mako, python-mock, python-openid, python-psutil, python-psycopg2,
 python-pybabel, python-pychart, python-pydot, python-pyparsing,
 python-reportlab, python-simplejson, python-tz, python-unittest2,
 python-vatnumber, python-vobject, python-webdav, python-werkzeug, python-xlwt,
 python-yaml, python-zsi,
 libjs-backbone,
Conflicts: openerp-server, openerp-web
Replaces: openerp-server, openerp-web
Recommends: graphviz, ghostscript, postgresql, python-matplotlib, poppler-utils
Description: Enterprise Resource Management
 OpenERP is a complete ERP (Enterprise Resource Planning) and CRM (Customer
 Relationship Management). The main features are accounting (analytic and
 financial), stock management, sales and purchases management, tasks automation,
 marketing campaigns, help desk, POS, etc. Technical features include a
 distributed server, flexible workflows, an object database, a dynamic GUI,
 customizable reports, and NET-RPC and XML-RPC interfaces.
